import { verify } from 'jsonwebtoken';
require('dotenv').config({ path: 'src/config/.env' });

const secret = process.env.SECRETKEY;

const verifyJWTToken = (token) => {
	const verifyToken = verify(token, secret);
	return verifyToken;
};

export default verifyJWTToken;
