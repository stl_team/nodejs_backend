import { Schema, model } from 'mongoose';

const userSchema = new Schema(
	{
		name: { type: String, index: true },
		email: { type: String, index: true },
		password: { type: String },
		role: {
			type: String,
			index: true,
			enum: ['SUPER_USER', 'ADMIN', 'USER'],
			default: 'USER',
		},

		token: { type: String },
		expTime: { type: Number },
		isDeleted: { type: Boolean, default: false, index: true },
	},
	{ timestamps: true, versionKey: false },
);

const User = new model('User', userSchema);

export default User;
