import express from 'express';
import userController from '../controllers/user.controller';
import validationSchema from '../joiSchema/validationSchema';
import authorize from '../middleware/authorization';

export default express
	.Router()
	.post('/signup', validationSchema.registerSchema, userController.signup)
	.get('/', authorize, userController.getAllUsers)
	.post('/login', validationSchema.loginSchema, userController.login);
