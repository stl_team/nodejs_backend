import { redBright, greenBright, yellowBright } from 'chalk';
import userModel from '../models/user.model';
import userService from '../mongoServices/user.services';
import hashPassword from '../utils/hashPassword';
import userdata from './JSONData/Users.json';

const seedUsers = async (data = userdata) => {
	try {
		let insertObj;
		data.forEach(async (element) => {
			console.info(yellowBright('Sowing seed for USERS'));
			const checkExistingUser = await userService.userQuery({
				email: element.email,
			});

			if (checkExistingUser) {
				console.error(
					redBright(
						'Sowing seed Error => \nUser Credentials Already In Use',
						JSON.stringify({
							email: checkExistingUser.email,
						}),
					),
				);
				return;
			}
			if (!checkExistingUser) {
				if (element.role === 'SUPER_USER') {
					insertObj = {
						...element,
						isActive: true,
					};
				}
				const hashed = await hashPassword(element.password);

				insertObj = {
					...insertObj,
					password: hashed,
				};
				const user = new userModel(insertObj);
				await user.save();
			}
			console.info(greenBright('Sowing seed completed for USERS'));
		});
	} catch (error) {
		console.error(redBright('error', error.message));
		return;
	}
};

seedUsers();

export default seedUsers;
