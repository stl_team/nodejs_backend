import userModel from '../models/user.model';
import comparePassword from '../utils/comparePassword';
import hashPassword from '../utils/hashPassword';
import generateJWT from '../utils/generateJWTToken';
import userService from '../mongoServices/user.services';

const signup = async (req, res) => {
	try {
		const { body } = req;
		const { email, password, role, firstName, lastName } = body;
		let message = 'User Registered Successfully.',
			data = body;
		const query = { email };

		// Check User Exists or Not
		const checkUser = await userService.userQuery(query);

		if (checkUser)
			throw {
				email: 'This email address is already in use',
			};

		// Password Hash
		const hashed = await hashPassword(password);

		// Check for Role ADMIN
		if (role === 'ADMIN') {
			message = `${role} Registered Successfully.`;
		}
		data = {
			...body,
			password: hashed,
		};

		// Save User into DB
		const user = new userModel(data);
		const saveUser = await user.save();

		if (!saveUser) throw new Error('Error While Saving User');
		saveUser &&
			res.status(200).send({
				success: true,
				message,
			});
	} catch (error) {
		res.status(400).send({
			success: false,
			message: error.message || error,
		});
	}
};

const getAllUsers = async (req, res) => {
	try {
		const { query } = req;
		const { totalCount, users } = await userService.findAllQuery(query);

		res.status(200).send({
			success: true,
			data: users,
			totalCount,
		});
	} catch (error) {
		res.status(400).send({
			success: false,
			message: error.message,
		});
	}
};

const login = async (req, res) => {
	try {
		const { body } = req;
		const { email, password } = body;
		let verifyPassword;

		// Check email Exists or Not
		const checkUser = await userService.userQuery({ email });

		if (!checkUser)
			throw new Error(`${email} Not Found. Please Register with Us!`);

		if (checkUser) {
			const {
				_id,
				role,
				isDeleted,
				firstName,
				lastName,
				screenName,
				email,
			} = checkUser;
			if (isDeleted) {
				throw new Error('Your Account is Temporary Deleted.');
			}

			// Generate JWT
			const token = generateJWT(_id);

			res.status(200).send({
				success: true,
				data: {
					token,
					_id,
					role,
					firstName,
					lastName,
					screenName,
					email,
				},
				message: `${role} Login Successfully`,
			});
		}
	} catch (error) {
		res.status(400).send({
			success: false,
			message: error.message,
		});
	}
};

export default {
	signup,
	getAllUsers,
	login,
};
