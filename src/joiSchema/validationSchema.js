import Joi from 'joi';
import validateRequest from '../middleware/validate-request';

const ROLE = ['SUPER_USER', 'ADMIN', 'USER'];

const firstName = Joi.string();
const lastName = Joi.string();
const email = Joi.string().email();
const password = Joi.string().min(6).max(25);
const token = Joi.string();

function registerSchema(req, res, next) {
	const schema = Joi.object({
		firstName: firstName.required(),
		lastName: lastName.required(),
		email: email.required(),
		role: Joi.string().valid('ADMIN', 'USER', 'SUPER_USER'),
		password: password.required(),
	});
	validateRequest(req, res, next, schema);
}

function loginSchema(req, res, next) {
	const schema = Joi.object({
		email: email.required(),
		password: password.required(),
	});
	validateRequest(req, res, next, schema);
}

export default {
	registerSchema,
	loginSchema,
};
